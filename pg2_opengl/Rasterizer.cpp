﻿#include "pch.h"
#include "Rasterizer.h"

Rasterizer::Rasterizer(int width, int height, float fov, Vector3 ViewFrom, Vector3 ViewAt, float fl1, float fl2)
{
	this->camera = Camera(width, height, fov, ViewFrom, ViewAt);
	//persp camera used for shadowing
	this->shadowCamera = Camera(640, 480, deg2rad(67.38f), Vector3(0.1, 0.1, 200), Vector3(0, 0, 0)); //spot light above world origin
	this->width = width;
	this->height = height;
	this->fov = fov;
}

Rasterizer::~Rasterizer()
{
}

void Rasterizer::LoadScene(const char * filename)
{
	LoadOBJ(filename, sceneSurfaces, sceneMaterials);

	for (Surface* surface : sceneSurfaces)
	{
		this->numOfTriangles += surface->no_triangles();
	}
}

int Rasterizer::InitDevice()
{
	glfwSetErrorCallback(glfw_callback);

	if (!glfwInit())
	{
		return(EXIT_FAILURE);
	}

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_SAMPLES, 8);
	glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);
	glfwWindowHint(GLFW_DOUBLEBUFFER, GL_TRUE);

	window = glfwCreateWindow(camera.width_, camera.height_, "PG2 OpenGL", nullptr, nullptr);
	if (!window)
	{
		glfwTerminate();
		return EXIT_FAILURE;
	}

	glfwSetFramebufferSizeCallback(window, framebuffer_resize_callback);
	glfwMakeContextCurrent(window);

	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		if (!gladLoadGL())
		{
			return EXIT_FAILURE;
		}
	}

	glEnable(GL_DEBUG_OUTPUT);
	glDebugMessageCallback(gl_callback, nullptr);

	printf("OpenGL %s, ", glGetString(GL_VERSION));
	printf("%s", glGetString(GL_RENDERER));
	printf(" (%s)\n", glGetString(GL_VENDOR));
	printf("GLSL %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	glEnable(GL_MULTISAMPLE);

	// map from the range of NDC coordinates <-1.0, 1.0>^2 to <0, width> x <0, height>
	glViewport(0, 0, camera.width_, camera.height_);
	// GL_LOWER_LEFT (OpenGL) or GL_UPPER_LEFT (DirectX, Windows) and GL_NEGATIVE_ONE_TO_ONE or GL_ZERO_TO_ONE
	glClipControl(GL_LOWER_LEFT, GL_NEGATIVE_ONE_TO_ONE);

	return S_OK;
}

int Rasterizer::InitPrograms(const char * vertexShader, const char * fragmentShader, GLuint& program)
{
	GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);

	const char* vertex_shader_source = LoadShader(vertexShader);

	glShaderSource(vertex_shader, 1, &vertex_shader_source, nullptr);
	glCompileShader(vertex_shader);
	SAFE_DELETE_ARRAY(vertex_shader_source);
	CheckShader(vertex_shader);

	GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);

	const char* fragment_shader_source = LoadShader(fragmentShader);

	glShaderSource(fragment_shader, 1, &fragment_shader_source, nullptr);
	glCompileShader(fragment_shader);
	SAFE_DELETE_ARRAY(fragment_shader_source);
	CheckShader(fragment_shader);

	program = glCreateProgram();
	glAttachShader(program, vertex_shader);
	glAttachShader(program, fragment_shader);
	glLinkProgram(program);

	glUseProgram(program);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);

	return S_OK;
}

int Rasterizer::InitMaterials(int i)
{
	#pragma pack( push, 1 ) // 1 B alignment
		struct GLMaterial
		{
			Color3f diffuse; // 3 * 4B
			GLbyte pad0[4]; // + 4 B = 16 B
			GLuint64 tex_diffuse_handle{ 0 }; // 1 * 8 B
			GLbyte pad1[8]; // + 8 B = 16 B
			Color3f rma; // 3 * 4 B
			GLbyte pad2[4]; // + 4 B = 16 B
		};
	#pragma pack( pop )

		GLMaterial * gl_materials = new GLMaterial[sceneMaterials.size()];
		int m = 0;
		for (const auto & material : sceneMaterials) {
			Texture3u* tex_diffuse = material->texture(Material::kDiffuseMapSlot);
			if (tex_diffuse) {
				GLuint id = 0;
				CreateBindlessTexture(id, gl_materials[m].tex_diffuse_handle, tex_diffuse->width(), tex_diffuse->height(), tex_diffuse->data());
				gl_materials[m].diffuse = Color3f({ 1.0f, 1.0f, 1.0f });  // white diffuse color
				gl_materials[m].rma = Color3f({ material->roughness(), material->metallicness, 1 });
			}
			else {
				GLuint id = 0;
				GLubyte data[] = { 255, 255, 255, 255 }; // opaque white
				CreateBindlessTexture(id, gl_materials[m].tex_diffuse_handle, 1, 1, data); // white texture
				gl_materials[m].diffuse = material->diffuse();
				gl_materials[m].rma = Color3f({ material->roughness(), material->metallicness, 1 });
			}
			m++;
		}
		GLuint ssbo_materials = 0;
		glGenBuffers(1, &ssbo_materials);
		glBindBuffer(GL_SHADER_STORAGE_BUFFER, ssbo_materials);
		const GLsizeiptr gl_materials_size = sizeof(GLMaterial) * sceneMaterials.size();
		glBufferData(GL_SHADER_STORAGE_BUFFER, gl_materials_size, gl_materials, GL_STATIC_DRAW);
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, ssbo_materials);
		glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);

		SAFE_DELETE_ARRAY(gl_materials);

	return S_OK;
}

void Rasterizer::MainLoop()
{
	float speedOfRotation = deg2rad(45);
	Matrix4x4 model;

	glUseProgram(shader_program);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, irradianceMap);
	SetSampler(shader_program, 0, "irradianceMap");

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, BRDFIntMap);
	SetSampler(shader_program, 1, "BRDFIntMap");

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, prefilteredEnvMap);
	SetSampler(shader_program, 2, "prefEnvMap");

	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, tex_shadow_map_);
	SetSampler(shader_program, 3, "shadowMap");

	Vector3 viewFrom = camera.view_from();
	SetVec3(shader_program, viewFrom.x, viewFrom.y, viewFrom.z, "cameraPosition");

	while (!glfwWindowShouldClose(window))
	{
		camera.MoveForward(0.001);
		camera.Update();
		model.set(0, 0, cosf(speedOfRotation));
		model.set(0, 1, -sinf(speedOfRotation));
		model.set(1, 0, sinf(speedOfRotation));
		model.set(1, 1, cosf(speedOfRotation));
		speedOfRotation += 0.0003;

		// --- first pass ---
		// set the shadow shader program and the viewport to match the size of the depth map
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		// set up the light source through the MLP matrix
		Matrix4x4 mlp = BuildMLPMatrix() * model;
		glUseProgram(shadow_program_);
		SetMatrix4x4(shadow_program_, mlp.data(), "MLP");
		glViewport(0, 0, shadow_width_, shadow_height_);
		glBindFramebuffer(GL_FRAMEBUFFER, fbo_shadow_map_);
			glClear(GL_DEPTH_BUFFER_BIT);
			// draw the scene
			glBindVertexArray(vao);
			glDrawArrays(GL_TRIANGLES, 0, numOfTriangles * 3);
			glBindVertexArray(0);
			// set back the main shader program and the viewport
		glBindFramebuffer(GL_FRAMEBUFFER, 0);

		//main shader
		glViewport(0, 0, camera.width_, camera.height_);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glUseProgram(shader_program);

		glClearColor(0.7f, 0.4f, 0.4f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT); // state using function

		Matrix4x4 mvp = camera.ProjectionMatrix * camera.ViewMatrix * model;

		SetMatrix4x4(shader_program, mvp.data(), "MVP");
		SetMatrix4x4(shader_program, model.data(), "MDL");
		SetMatrix4x4(shader_program, mlp.data(), "MLP");

		glBindVertexArray(vao);
		glDrawArrays(GL_TRIANGLES, 0, numOfTriangles * 3);
		glBindVertexArray(0);

		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	glDeleteShader(vertex_shader);
	glDeleteShader(fragment_shader);
	glDeleteProgram(shader_program);
	glDeleteVertexArrays(1, &vao);
	glDeleteBuffers(1, &vbo);

	glfwTerminate();
}

void Rasterizer::InitPrefilteredEnvMap(const std::vector<std::string> filenames)
{
	const GLint max_level = GLint(filenames.size()) - 1; // assume we have a list of images representing different levels of a map
	glGenTextures(1, &prefilteredEnvMap);
	glBindTexture(GL_TEXTURE_2D, prefilteredEnvMap);
	if (glIsTexture(prefilteredEnvMap)) {
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, max_level);
		int width, height;

		GLint level;
		for (level = 0; level < GLint(filenames.size()); ++level) {
			Texture3f prefiltered_env_map = Texture3f(filenames[level]);
			glTexImage2D(GL_TEXTURE_2D, level, GL_RGB32F, prefiltered_env_map.width(), prefiltered_env_map.height(), 0, GL_RGB, GL_FLOAT,
				prefiltered_env_map.data());
			width = prefiltered_env_map.width() / 2;
			height = prefiltered_env_map.height() / 2;
		}
	}
	glBindTexture(GL_TEXTURE_2D, 0);
}

void Rasterizer::InitIrradianceMap(const char * filename)
{
	Texture3f* bitmap = new Texture3f(filename);

	glGenTextures(1, &irradianceMap);
	glBindTexture(GL_TEXTURE_2D, irradianceMap);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, bitmap->width(), bitmap->height(), 0, GL_RGB, GL_FLOAT, bitmap->data()); // note how we specify the texture's data value to be float
	glGenerateMipmap(GL_TEXTURE_2D);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glBindTexture(GL_TEXTURE_2D, 0);
}

void Rasterizer::InitGGXIntegrMap(const char * filename)
{
	Texture3f* bitmap = new Texture3f(filename);

	glGenTextures(1, &BRDFIntMap);
	glBindTexture(GL_TEXTURE_2D, BRDFIntMap);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, bitmap->width(), bitmap->height(), 0, GL_RGB, GL_FLOAT, bitmap->data());
	glGenerateMipmap(GL_TEXTURE_2D);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glBindTexture(GL_TEXTURE_2D, 0);
}

int Rasterizer::InitBuffers() {
	const int no_vertices = this->numOfTriangles * 3;
	Vertex* vertices = new Vertex[no_vertices];

	const int vertex_stride = sizeof(Vertex);
	Vertex vertex;
	const size_t offset0 = (BYTE*)&vertex.position - (BYTE*)(&vertex);
	const size_t offset1 = (BYTE*)&vertex.normal - (BYTE*)(&vertex);
	const size_t offset2 = (BYTE*)&vertex.color - (BYTE*)(&vertex);
	const size_t offset3 = (BYTE*)&vertex.texture_coords[0] - (BYTE*)(&vertex);
	const size_t offset4 = (BYTE*)&vertex.tangent - (BYTE*)(&vertex);
	const size_t offset5 = (BYTE*)&vertex.material_index - (BYTE*)(&vertex);

	int k = 0;
	for (auto surface : sceneSurfaces)
	{
		// triangles loop
		for (int i = 0; i < surface->no_triangles(); ++i)
		{
			Triangle & triangle = surface->get_triangle(i);
			// vertices loop
			for (int j = 0; j < 3; ++j, ++k)
			{
				Vertex v = triangle.vertex(j);
				v.material_index = surface->get_material()->material_index;
				vertices[k] = v;
				//sample(v.normal.x, v.normal.y, v.normal.z);
			} // end of vertices loop

		} // end of triangles loop

	} // end of surfaces loop

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(1, &vbo); // generate vertex buffer object (one of OpenGL objects) and get the unique ID corresponding to that buffer
	glBindBuffer(GL_ARRAY_BUFFER, vbo); // bind the newly created buffer to the GL_ARRAY_BUFFER target
	glBufferData(GL_ARRAY_BUFFER, no_vertices * sizeof(Vertex), vertices, GL_STATIC_DRAW); // copies the previously defined vertex data into the buffer's memory
																			   // vertex position
	//vertex position
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, vertex_stride, (void*)offset0);
	glEnableVertexAttribArray(0);
	//vertex normal
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, vertex_stride, (void*)offset1);
	glEnableVertexAttribArray(1);
	//vertex color
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, vertex_stride, (void*)offset2);
	glEnableVertexAttribArray(2);
	//vertex texture_coords
	glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, vertex_stride, (void*)offset3);
	glEnableVertexAttribArray(3);
	//vertex tangent
	glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, vertex_stride, (void*)offset4);
	glEnableVertexAttribArray(4);
	//vertex material_index
	glVertexAttribIPointer(5, 1, GL_INT, vertex_stride, (void*)offset5);
	glEnableVertexAttribArray(5);

	delete[] vertices;
	return S_OK;
}

int Rasterizer::InitShadowDepthbuffer()
{
	glGenTextures(1, &tex_shadow_map_); // texture to hold the depth values from the light's perspective
	glBindTexture(GL_TEXTURE_2D, tex_shadow_map_);
	// GL_DEPTH_COMPONENT ... each element is a single depth value. The GL converts it to floating point, multiplies by the signed scale
	// factor GL_DEPTH_SCALE, adds the signed bias GL_DEPTH_BIAS, and clamps to the range [0, 1] – this will be important later
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, shadow_width_, shadow_height_, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
	const float color[] = { 1.0f, 1.0f, 1.0f, 1.0f }; // areas outside the light's frustum will be lit
	glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, color);
	glBindTexture(GL_TEXTURE_2D, 0);
	glGenFramebuffers(1, &fbo_shadow_map_); // new frame buffer
	glBindFramebuffer(GL_FRAMEBUFFER, fbo_shadow_map_);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, tex_shadow_map_, 0); // attach the texture as depth
	glDrawBuffer(GL_NONE); // we dont need any color buffer during the first pass
	glBindFramebuffer(GL_FRAMEBUFFER, 0); // bind the default framebuffer back

	return 0;
}

Matrix4x4 Rasterizer::BuildMLPMatrix()
{	
	return shadowCamera.ProjectionMatrix * shadowCamera.ViewMatrix;
}

bool check_gl(const GLenum error)
{
	if (error != GL_NO_ERROR)
	{
		//const GLubyte * error_str;
		//error_str = gluErrorString( error );
		//printf( "OpenGL error: %s\n", error_str );

		return false;
	}

	return true;
}

/* glfw callback */
void glfw_callback(const int error, const char * description)
{
	printf("GLFW Error (%d): %s\n", error, description);
}

/* OpenGL messaging callback */
void GLAPIENTRY gl_callback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar * message, const void * user_param)
{
	printf("GL %s type = 0x%x, severity = 0x%x, message = %s\n",
		(type == GL_DEBUG_TYPE_ERROR ? "Error" : "Message"),
		type, severity, message);
}

/* invoked when window is resized */
void framebuffer_resize_callback(GLFWwindow * window, int width, int height)
{
	glViewport(0, 0, width, height);
}

/* load shader code from text file */
char * LoadShader(const char * file_name)
{
	FILE * file = fopen(file_name, "rt");

	if (file == NULL)
	{
		printf("IO error: File '%s' not found.\n", file_name);

		return NULL;
	}

	size_t file_size = static_cast<size_t>(GetFileSize64(file_name));
	char * shader = NULL;

	if (file_size < 1)
	{
		printf("Shader error: File '%s' is empty.\n", file_name);
	}
	else
	{
		/* v glShaderSource nezadáváme v posledním parametru délku,
		takže řetězec musí být null terminated, proto +1 a reset na 0*/
		shader = new char[file_size + 1];
		memset(shader, 0, sizeof(*shader) * (file_size + 1));

		size_t bytes = 0; // počet již načtených bytů

		do
		{
			bytes += fread(shader, sizeof(char), file_size, file);
		} while (!feof(file) && (bytes < file_size));

		if (!feof(file) && (bytes != file_size))
		{
			printf("IO error: Unexpected end of file '%s' encountered.\n", file_name);
		}
	}

	fclose(file);
	file = NULL;

	return shader;
}

/* check shader for completeness */
GLint CheckShader(const GLenum shader)
{
	GLint status = 0;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &status);

	printf("Shader compilation %s.\n", (status == GL_TRUE) ? "was successful" : "FAILED");

	if (status == GL_FALSE)
	{
		int info_length = 0;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &info_length);
		char * info_log = new char[info_length];
		memset(info_log, 0, sizeof(*info_log) * info_length);
		glGetShaderInfoLog(shader, info_length, &info_length, info_log);

		printf("Error log: %s\n", info_log);

		SAFE_DELETE_ARRAY(info_log);
	}

	return status;
}
