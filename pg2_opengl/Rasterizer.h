#pragma once
#include "camera.h"
#include "objloader.h"
#include "utils.h"
#include "glutils.h"
#include "mymath.h"
#include <iostream>
#include <vector>
#include <string>
#include "texture.h"

class Rasterizer {
public:
	Rasterizer(int width, int height, float fov, Vector3 ViewFrom, Vector3 ViewAt, float fl1, float fl2);
	~Rasterizer();
	void LoadScene(const char * filename);
	int InitDevice();
	int InitBuffers();
	int InitPrograms(const char* vertexShader, const char* fragmentShader, GLuint &shader_program);
	int InitMaterials(int i);
	void MainLoop();
	void InitPrefilteredEnvMap(const std::vector<std::string> filenames);
	void InitIrradianceMap(const char * filename);
	void InitGGXIntegrMap(const char * filename);
	int InitShadowDepthbuffer(); // initialize shadow (depth) map texture and framebuffer for the first pass
	Matrix4x4 BuildMLPMatrix();
	GLuint shader_program;
	GLuint shader_debug_program;
	GLuint shadow_program_{ 0 }; // collection of shadow mapping shaders
private:
	Camera camera;
	Camera shadowCamera;
	GLFWwindow * window;
	int no_triangles;
	GLuint fragment_shader{ 0 };
	GLuint vertex_shader{ 0 };
	GLuint vao{ 0 };
	GLuint vbo{ 0 };
	int numOfTriangles, numOfSurfaces;
	std::vector<Surface *> sceneSurfaces;
	std::vector<Material *> sceneMaterials;
	int width, height;
	float fov, fl1, fl2;
	Vector3 ViewFrom, ViewAt;
	unsigned int irradianceMap;
	unsigned int prefilteredEnvMap;
	unsigned int BRDFIntMap;

	int shadow_width_{ 1024 }; // shadow map resolution
	int shadow_height_{ shadow_width_ };
	GLuint fbo_shadow_map_{ 0 }; // shadow mapping FBO
	GLuint tex_shadow_map_{ 0 }; // shadow map texture
};

bool check_gl(const GLenum error);

void glfw_callback(const int error, const char * description);

void GLAPIENTRY gl_callback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar * message, const void * user_param);

void framebuffer_resize_callback(GLFWwindow * window, int width, int height);

char * LoadShader(const char * file_name);

GLint CheckShader(const GLenum shader);
