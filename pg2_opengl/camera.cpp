#include "pch.h"
#include "camera.h"

Camera::Camera(const int width, const int height, const float fov_y,
	const Vector3 view_from, const Vector3 view_at)
{
	width_ = width;
	height_ = height;
	fov_y_ = fov_y;

	view_from_ = view_from;
	view_at_ = view_at;

	Update();
}

Vector3 Camera::view_from() const
{
	return view_from_;
}

Matrix3x3 Camera::M_c_w() const
{
	return M_c_w_;
}

float Camera::focal_length() const
{
	return f_y_;
}

void Camera::set_fov_y(const float fov_y)
{
	assert(fov_y > 0.0);

	fov_y_ = fov_y;
}

void Camera::Update()
{
	f_y_ = height_ / (2.0f * tanf(fov_y_ * 0.5f));

	Vector3 z_c = view_from_ - view_at_;
	z_c.Normalize();
	Vector3 x_c = up_.CrossProduct(z_c);
	x_c.Normalize();
	Vector3 y_c = z_c.CrossProduct(x_c);
	y_c.Normalize();
	M_c_w_ = Matrix3x3(x_c, y_c, z_c);

	Matrix4x4 V1 = Matrix4x4(x_c, y_c, z_c, view_from_);
	V1.EuclideanInverse();
	this->ViewMatrix = V1;

	int n = 1;
	int f = 1000.f;

	int a = (n + f) / (n - f);
	int b = 2 * n * f / (n - f);

	float aspect = width_ / float(height_);
	float fov_x_ = 2 * atan(1 / aspect * tan(fov_y_ / 2));
	float w = 2 * n * tan(fov_y_ / 2);
	float h = w * (1 / aspect);

	//Normalization transformation - pdf slide 26
	Matrix4x4 N = Matrix4x4(
		2 / w, 0, 0, 0,
		0, 2 / h, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1);

	//Projection transform
	Matrix4x4 M = Matrix4x4(
		n, 0, 0, 0,
		0, n, 0, 0,
		0, 0, a, b,
		0, 0, -1, 0);

	//Perspective projection matrix
	Matrix4x4 P = N * M;
	this->ProjectionMatrix = P;
}

void Camera::MoveForward(const float dt)
{
	Vector3 ds = view_at_ - view_from_;
	ds.Normalize();
	ds *= dt;

	view_from_ += ds;
	view_at_ += ds;
}
