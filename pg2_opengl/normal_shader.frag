#version 450 core

in vec3 unified_normal;

out vec4 FragColor;

void main(void)
{
	FragColor = vec4((unified_normal.x + 1) * 0.5, (unified_normal.y + 1) * 0.5, (unified_normal.z + 1) * 0.5, 1.0f);
}