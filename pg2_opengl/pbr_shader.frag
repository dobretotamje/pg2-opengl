#version 460 core
#extension GL_ARB_bindless_texture : require

in vec2 texcoord;
in vec4 fragColor;
in vec3 normal;
in vec3 position_ms;
in vec3 position_lcs;

uniform sampler2D shadowMap;
uniform sampler2D irradianceMap;
uniform sampler2D BRDFIntMap;
uniform sampler2D prefEnvMap;
uniform vec3 cameraPosition;

const int maxLevel = 7;

flat in int material_index;

struct Material
{
	vec3 diffuse;
	uvec2 tex_diffuse;
	vec3 rma;
};

layout (std430, binding = 0) readonly buffer Materials
{
	Material materials[]; // only the last member can be unsized
};

const vec2 invAtan = vec2(0.1591, 0.3183);
vec2 SampleSphericalMap(vec3 v)
{
    vec2 uv = vec2(atan(v.z, v.x), asin(v.y));
    uv *= invAtan;
    uv += 0.5;
    return uv;
}

const float gamma = 2.2;
vec4 GammaCorrection(vec4 color) {
	// Exposure tone mapping
	vec3 mapped = vec3(1.0) - exp(-color.rgb);
	// Gamma correction 
	mapped = pow(mapped, vec3(1.0 / gamma));
	return vec4(mapped, 1.0);
}

float fresnelSchlick(float cosTheta, float F0)
{
    return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
}

void main(void)
{	
	//shadows
	// convert LCS's range <-1, 1> into TC's range <0, 1>
	vec2 a_tc = ( position_lcs.xy + vec2( 1.0f ) ) * 0.5f;
	float depth = texture( shadowMap, a_tc ).r;
	float bias = 0.001;
	// (pseudo)depth was rescaled from <-1, 1> to <0, 1>
	depth = depth * 2.0f - 1.0f; // we need to do the inverse
	float shadow = depth + bias >= position_lcs.z ? 1.0f : 0.15f;


	sampler2D texSampler = sampler2D(materials[material_index].tex_diffuse);

	vec4 albedo = vec4(materials[material_index].diffuse.rgb * texture(texSampler, texcoord).rgb, 1.0f);

	// diffuse part
	vec2 uv = SampleSphericalMap(normalize(normal));

	vec3 exrIrradianceColor = texture(irradianceMap, uv).rgb;

	//specular part

	float roughness = materials[material_index].rma.r;
	vec3 V = normalize(cameraPosition - position_ms);
	float cosTheta = max(dot(normal, V), 0.0);

	//float ior = 1.460; //plastic
	//float F0 = pow((1 - ior) / (1 + ior), 2);
	//float Ks = fresnelSchlick(cosTheta, F0);
	float Ks = 0.3;
	//float metalness = 0.5;
	//float Kd = (1 - Ks) * (1 - metalness);
	float Kd = 1;

	//vec3 W0 = vec3(sqrt(1 - (cosTheta * cosTheta)), 0, cosTheta);
	vec4 sb = texture(BRDFIntMap, vec2(cosTheta, roughness));
	float s = sb.x;
	float b = sb.y;

	vec3 WI = normalize(reflect(V, normal));

	vec4 specularPart = textureLod(prefEnvMap, SampleSphericalMap(WI), roughness * maxLevel);

	vec4 diffusePart = albedo * vec4(exrIrradianceColor, 1.0);

	vec4 toneUnmappedColor = ((Kd * diffusePart) + (((Ks * s) + b) * specularPart)) * shadow;

	gl_FragColor = GammaCorrection(toneUnmappedColor);
}