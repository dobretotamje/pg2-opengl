#version 460 core
layout(location = 0) in vec4 in_position_ms; 
layout(location = 1) in vec3 in_normal_ms;
layout(location = 2) in vec3 in_color;
layout(location = 3) in vec2 in_texcoord;
layout(location = 4) in vec3 in_tangent;
layout(location = 5) in int in_material_index;

uniform mat4 MVP; //model view projection
uniform mat4 MDL;
uniform mat4 MLP; //model light projection matrix

out vec2 texcoord;
out vec4 fragColor;
out vec3 position_ms;
flat out int material_index;
out vec3 normal;
out vec3 position_lcs; // this is our point a (or b) in lcs

void main(void) {
	vec4 tmp = MLP * vec4(in_position_ms.xyz, 1.0f);
	position_lcs = tmp.xyz / tmp.w;

	//model space -> clip space
	gl_Position = MVP * vec4(in_position_ms.x, in_position_ms.y, in_position_ms.z, 1.0f);

	//3ds max fix
	texcoord = vec2(in_texcoord.x, 1.0f - in_texcoord.y); 
	material_index = in_material_index;

	normal = normalize(mat3(MDL) * in_normal_ms);

	position_ms = (MDL * in_position_ms).xyz;
}
