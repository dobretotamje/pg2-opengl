#include "pch.h"
#include "Rasterizer.h"
#include "mymath.h"
#include "pg2_opengl.h"

int main()
{
	printf("PG2 OpenGL, (c)2019 Tomas Fabian\n\n");

	//avenger
	//Rasterizer rasterizer(640, 480, deg2rad(67.38f), Vector3(130, -120, 100),
	//	Vector3(0, 0, 35), 1.0f, 1000.0f);

	//kostka
	Rasterizer rasterizer(640, 480, deg2rad(67.38f), Vector3(20, 20, 20),
		Vector3(0, 0, 0), 1.0f, 1000.0f); //z = 35 avenger

	rasterizer.InitDevice(); // inicializace OpenGL kontext
	//rasterizer.LoadScene("data\\6887_allied_avenger_gi.obj"); // na�ten� geometrie sc�ny
	rasterizer.LoadScene("data\\kostka\\piece_02.obj");
	rasterizer.InitBuffers(); // inicializace VAO a VBO
	//rasterizer.InitPrograms("normal_shader.vert", "normal_shader.frag", rasterizer.shader_program); // inicializace VS a FS shader� (programu)
	rasterizer.InitPrograms("pbr_shader.vert", "pbr_shader.frag", rasterizer.shader_program);
	rasterizer.InitPrograms("shadow_shader.vert", "shadow_shader.frag", rasterizer.shadow_program_);
	//rasterizer.InitPrograms("simply_shader.vert", "simply_shader.frag", rasterizer.shader_program);
	//rasterizer.InitPrograms("depth_debug_shader.vert", "depth_debug_shader.frag", rasterizer.shader_debug_program);
	rasterizer.InitMaterials(0); // inicializace SSBO pro ulo�en� materi�l�
	// n�sleduje inicializace p�edpo��tan�ch map pro Cook-Torrance GGX shader
	rasterizer.InitIrradianceMap("data\\lebombo_irradiance_map.exr");
	rasterizer.InitPrefilteredEnvMap({
		"data\\lebombo_prefiltered_env_map_001_2048.exr",
		"data\\lebombo_prefiltered_env_map_010_1024.exr",
		"data\\lebombo_prefiltered_env_map_100_512.exr",
		"data\\lebombo_prefiltered_env_map_250_256.exr",
		"data\\lebombo_prefiltered_env_map_500_128.exr",
		"data\\lebombo_prefiltered_env_map_750_64.exr",
		"data\\lebombo_prefiltered_env_map_999_32.exr" }
	);
	rasterizer.InitGGXIntegrMap("data\\brdf_integration_map_ct_ggx.png");
	rasterizer.InitShadowDepthbuffer();
	rasterizer.MainLoop(); // renderovac� smy�ka
	return EXIT_SUCCESS;
}