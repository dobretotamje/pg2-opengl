#version 460 core

layout ( location = 0 ) in vec4 in_position_ms;

// Projection (P_l)*Light (V_l)*Model (M) matrix
uniform mat4 MLP;

void main( void )
{
	gl_Position = MLP * in_position_ms;
}