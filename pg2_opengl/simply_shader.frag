#version 460 core

out vec4 FragColor;

void main(void)
{
	FragColor = vec4(0.5, 0.25, 0.75, 1.0f);
}