#version 460 core
layout(location = 0) in vec4 in_position_ms; 

uniform mat4 MVP; //model view projection

void main(void)
{
	gl_Position = MVP * vec4(in_position_ms.x, in_position_ms.y, in_position_ms.z, 1.0f);
}